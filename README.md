# Project description
This is my authored application implemented in Java. This application can be used by amateur pilot. It allows you to create account, airplanes, check weather or check information about airport.

The operations that can be performed are:
- register user with confirmation on email 
- reset password for user
- resend registration link
- login (JWT token implemented)
- get current weather by city or coordinates from OPEN WEATHER MAP service
- get forecast weather by city or coordinates from OPEN WEATHER MAP service
- get decoded/encoded METAR or TAF from CHECK WX service
- CRUD operations on airplane
- get information about airport from AERO DATA BOX service
# Project objective
The main purpose of this project was to play a little bit with Spring and Hibernate frameworks, learn how to design architecture for simple RESTful web service. Learn how to write REST Controller classes and how to send HTTP requests in Java code.
# Test it live
The application has been deployed on [Heroku](https://www.heroku.com/) cloud . You can test it by yourself with [POSTMAN](https://www.getpostman.com) or this [website](https://airlines-router-93577.herokuapp.com/airports/icao/epgd).
List of available endpoints under links [Swagger](https://airlines-router-93577.herokuapp.com/swagger-ui.html#/)
