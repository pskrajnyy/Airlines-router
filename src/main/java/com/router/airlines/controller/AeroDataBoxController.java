package com.router.airlines.controller;

import com.router.airlines.client.aeroDataBox.BasicInformationAirport;
import com.router.airlines.client.aeroDataBox.LocalTimeAirport;
import com.router.airlines.client.aeroDataBox.RunwaysInformationAirport;
import com.router.airlines.dto.aeroDataBox.AirportBasicInformationDto;
import com.router.airlines.dto.aeroDataBox.LocalTimeAirportDto;
import com.router.airlines.dto.aeroDataBox.RunwaysDto;
import com.router.airlines.exception.AirportNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/airports")
public class AeroDataBoxController {

    @Autowired
    BasicInformationAirport basicInformationAirport;

    @Autowired
    LocalTimeAirport localTimeAirport;

    @Autowired
    RunwaysInformationAirport runwaysInformationAirport;

    @GetMapping("icao/{icao}")
    public ResponseEntity<AirportBasicInformationDto> getBasicInformationAboutAirportByIcao(@PathVariable String icao) throws AirportNotFoundException {
        log.info("Get basic information about airport: {}", icao);
        try {
            AirportBasicInformationDto airportBasicInformationDto = basicInformationAirport.getBasicInformationAboutAirport("icao", icao);
            return new ResponseEntity<>(airportBasicInformationDto, HttpStatus.OK);
        } catch (Exception e) {
            throw new AirportNotFoundException("Airport with " + icao + " code not found");
        }
    }

    @GetMapping("iata/{iata}")
    public ResponseEntity<AirportBasicInformationDto> getBasicInformationAboutAirportByIata(@PathVariable String iata) throws AirportNotFoundException {
        log.info("Get basic information about airport: {}", iata);
        try {
            AirportBasicInformationDto airportBasicInformationDto = basicInformationAirport.getBasicInformationAboutAirport("iata", iata);
            return new ResponseEntity<>(airportBasicInformationDto, HttpStatus.OK);
        } catch (Exception e) {
            throw new AirportNotFoundException("Airport with " + iata + " code not found");
        }
    }

    @GetMapping("{icao}/time/local")
    public ResponseEntity<LocalTimeAirportDto> getLocalTimeAirportByIcao(@PathVariable String icao) throws AirportNotFoundException {
        log.info("Get local time on airport: {}", icao);
        try {
            LocalTimeAirportDto localTimeAirportDto = localTimeAirport.getLocalTimeAirport(icao);
            return new ResponseEntity<>(localTimeAirportDto, HttpStatus.OK);
        } catch (Exception e) {
            throw new AirportNotFoundException("Airport with " + icao + " code not found");
        }
    }

    @GetMapping("{icao}/runways")
    public ResponseEntity<RunwaysDto[]> getRunwayInformationAirportByIcao(@PathVariable String icao) throws AirportNotFoundException {
        log.info("Get information about runways on airport: {}", icao);
        try {
            RunwaysDto[] runwaysDtos = runwaysInformationAirport.getRunwayInformationAirportByIcao(icao);
            return new ResponseEntity<>(runwaysDtos, HttpStatus.OK);
        } catch (Exception e) {
            throw new AirportNotFoundException("Airport with " + icao + " code not found");
        }
    }
}
