package com.router.airlines.controller;

import com.router.airlines.service.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

@Controller
public class EmailController {

    @Autowired
    private MessageSource messages;

    private final EmailService emailService;

    private final TemplateEngine templateEngine;

    @Autowired
    public EmailController(EmailService emailService, TemplateEngine templateEngine) {
        this.emailService = emailService;
        this.templateEngine = templateEngine;
    }

    public String sendResetPasswordToken(String contextPath, String token, String to, Long userId) {
        Context context = new Context();
        context.setVariable("header", "Reset Password");
        context.setVariable("title", "You can reset your password by below button:");
        String url = contextPath + "/user/changePassword?id=" + userId + "&token=" + token;
        context.setVariable("description", url);

        String body = templateEngine.process("tokenEmailTemplate", context);
        emailService.sendEmail(to, "Reset Password", body);
        return null;
    }

    public String sendNewVerificationToken(String contextPath, String token, String to) {
        Context context = new Context();
        context.setVariable("header", "New verification token");
        context.setVariable("title", "You can activate your account by below button:");
        String url = contextPath + "/registrationConfirm?token=" + token;
        context.setVariable("description", url);

        String body = templateEngine.process("tokenEmailTemplate", context);
        emailService.sendEmail(to, "New verification token", body);
        return null;
    }

    public String sendVerificationToken(String contextPath, String token, String to) {
        Context context = new Context();
        context.setVariable("header", "Verification token");
        context.setVariable("title", "You can activate your account by below button:");
        String url = contextPath + "/registrationConfirm?token=" + token;
        context.setVariable("description", url);

        String body = templateEngine.process("tokenEmailTemplate", context);
        emailService.sendEmail(to, "New verification token", body);
        return null;
    }
}
