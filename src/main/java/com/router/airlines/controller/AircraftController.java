package com.router.airlines.controller;

import com.router.airlines.domain.Aircraft;
import com.router.airlines.dto.AircraftDto;
import com.router.airlines.exception.AircraftNotFoundException;
import com.router.airlines.service.implementation.AircraftServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.type.ListType;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.Type;
import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Slf4j
@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/aircraft")
public class AircraftController {

//    @GetMapping("/{reportTranslationId}")
//    public ResponseEntity<RptReportTranslation> getById(@PathVariable Integer reportTranslationId) {
//        RptReportTranslation translation = service.selectById(reportTranslationId);
//
//        return new ResponseEntity<>(translation, HttpStatus.OK);
//    }


    @Autowired
    private AircraftServiceImpl aircraftService;

    private ModelMapper mapper = new ModelMapper();

    @PostMapping(consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> createAircraft(@RequestBody AircraftDto aircraftDto) {
        log.info("Create new aircraft {}", aircraftDto.getModel());
        aircraftService.saveAircraft(mapper.map(aircraftDto, Aircraft.class));
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @GetMapping("{aircraftId}")
    public ResponseEntity<AircraftDto> getAircraft(@PathVariable Long aircraftId) {
        Aircraft aircraft = aircraftService.getAircraft(aircraftId);
        if(aircraft != null) {
            log.info("Get aircraft by ID = {}", aircraftId);
            AircraftDto aircraftDto = mapper.map(aircraft, AircraftDto.class);
            return new ResponseEntity<>(aircraftDto, HttpStatus.OK);
        }
        throw new AircraftNotFoundException("Aircraft with ID = " + aircraftId + " not exists");
    }

    @GetMapping("all")
    public ResponseEntity<List<AircraftDto>> getAllAircraft() {
        log.info("Get list of aircrafts");
        List<Aircraft> aircraftList = aircraftService.getAllAircrafts();
        Type listType = new TypeToken<List<AircraftDto>>(){}.getType();
        List<AircraftDto> aircraftDtoList = mapper.map(aircraftList, listType);
        return new ResponseEntity<>(aircraftDtoList, HttpStatus.OK);
    }

    @DeleteMapping("{aircraftId}")
    public ResponseEntity<Void> deleteAircraftById(@PathVariable Long aircraftId) {
        log.info("Delete aircraft by ID = {}", aircraftId);
        aircraftService.deleteById(aircraftId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PutMapping
    public ResponseEntity<AircraftDto> updateAircraft(@RequestBody AircraftDto aircraftDto) {
        log.info("Update the aircraft with ID = {}", aircraftDto.getId());
        AircraftDto updatedAircraft = mapper.map(aircraftService.saveAircraft(mapper.map(aircraftDto, Aircraft.class)), AircraftDto.class);
        return new ResponseEntity<>(updatedAircraft, HttpStatus.OK);
    }
}
