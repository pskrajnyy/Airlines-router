package com.router.airlines.controller;

import com.router.airlines.config.jwt.JwtProvider;
import com.router.airlines.domain.User;
import com.router.airlines.domain.VerificationToken;
import com.router.airlines.dto.UserDto;
import com.router.airlines.exception.UserIsEnabledException;
import com.router.airlines.exception.UserNotFoundException;
import com.router.airlines.exception.VerificationTokenExpiredException;
import com.router.airlines.exception.VerificationTokenInvalidException;
import com.router.airlines.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.UUID;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Slf4j
@RestController
@RequestMapping("register")
@CrossOrigin(origins = "*")
public class RegistrationController {

    @Autowired
    private UserService userService;

    @Autowired
    private EmailController emailController;

    @Qualifier("messageSource")
    @Autowired
    private MessageSource messages;

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    JwtProvider jwtProvider;

    public RegistrationController() {
        super();
    }

    @PostMapping(value = "/registration", consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<String> registerUserAccount(@RequestBody UserDto accountDto, final HttpServletRequest request) {
        log.info("Registering user account with information: {}", accountDto);

        final User registered = userService.registerNewUserAccount(accountDto);
        final String token = UUID.randomUUID().toString();
        userService.createVerificationTokenForUser(registered, token);
        emailController.sendVerificationToken(getAppUrl(request), token, registered.getEmail());
        return new ResponseEntity<>(messages.getMessage("message.regSucc", null, null), HttpStatus.OK);
    }

    @GetMapping(value = "/registrationConfirm")
    public ResponseEntity<String> confirmRegistration(@RequestParam("token") String token) {
        final String result = userService.validateVerificationToken(token);
        if (result.equals("expired")) {
            throw new VerificationTokenExpiredException("This verification token is expired : " + token);
        } else if (result.equals("invalidToken")) {
            throw new VerificationTokenInvalidException("This verification token is invalid : " + token);
        } else if (result.equals("valid")) {
            log.info("Registration confirm with token: " + token);
            return new ResponseEntity<>(messages.getMessage("message.accountVerified", null, null), HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @GetMapping(value = "/resendRegistrationToken")
    public ResponseEntity<String> resendRegistrationToken(@RequestParam("email") String email, final HttpServletRequest request) {
        final User user = userService.findUserByEmail(email);

        if (user == null) {
            throw new UserNotFoundException("Account for " + email + " email not exists");
        }

        if (user.isEnabled()) {
            throw new UserIsEnabledException("Account with " + email + " email is enabled");
        }

        String existingToken = userService.getVerificationTokenByUser(user).getToken();
        final VerificationToken newToken = userService.generateNewVerificationToken(existingToken);

        emailController.sendNewVerificationToken(getAppUrl(request), newToken.getToken(), user.getEmail());
        log.info("Send new registration token for email: {}", email);
        return new ResponseEntity<>(messages.getMessage("message.resendToken", null, null), HttpStatus.OK);
    }

    @GetMapping(value = "/resetPassword")
    public ResponseEntity<String> resetPassword(@RequestParam("email") final String userEmail, final HttpServletRequest request) {
        final User user = userService.findUserByEmail(userEmail);
        if (user != null) {
            final String token = UUID.randomUUID().toString();
            userService.createPasswordResetTokenForUser(user, token);
            emailController.sendResetPasswordToken(getAppUrl(request), token, user.getEmail(), user.getId());
            return new ResponseEntity<>(messages.getMessage("message.resetPasswordSucc", null, null), HttpStatus.OK);
        }
        throw new UserNotFoundException("Account for " + userEmail + " email not exists");
    }

    private String getAppUrl(HttpServletRequest request) {
        return "http://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
    }
}
