package com.router.airlines.controller;

import com.router.airlines.config.jwt.JwtProvider;
import com.router.airlines.config.jwt.JwtResponse;
import com.router.airlines.domain.User;
import com.router.airlines.exception.UserNotFoundException;
import com.router.airlines.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@CrossOrigin(origins = "*")
public class LoginController {

    @Autowired
    private UserService userService;

    @Qualifier("messageSource")
    @Autowired
    private MessageSource messages;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    JwtProvider jwtProvider;

    @PostMapping(value = "/login")
    public ResponseEntity<?> login(@RequestParam("email") final String email, @RequestParam("password") final String password) {
        final User user = userService.findUserByEmail(email);
        if (user != null) {
            boolean result = passwordEncoder.matches(password, user.getPassword());
            if (!result) {
                return new ResponseEntity<>(messages.getMessage("auth.message.invalidPassword", null, null), HttpStatus.OK);
            }

            if (!user.isEnabled()) {
                return new ResponseEntity<>(messages.getMessage("message.userIsDisabled", null, null), HttpStatus.OK);
            }

            Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(email, password)
            );
            SecurityContextHolder.getContext().setAuthentication(authentication);

            String jwt = jwtProvider.generateJwtToken(authentication);
            return ResponseEntity.ok(new JwtResponse(jwt));
        }
        throw new UserNotFoundException("Account for " + email + " email not exists");
    }
}
