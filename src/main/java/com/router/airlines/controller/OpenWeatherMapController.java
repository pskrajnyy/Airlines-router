package com.router.airlines.controller;

import com.router.airlines.client.openWeatherMap.CurrentWeatherOneLocation;
import com.router.airlines.client.openWeatherMap.HourlyForecastOneLocation;
import com.router.airlines.dto.openWeatherMap.OpenWeatherMapCurrentDto;
import com.router.airlines.dto.openWeatherMap.OpenWeatherMapForecastDto;
import com.router.airlines.exception.CityNotFoundException;
import com.router.airlines.exception.WeatherForCoordinatesNotAvailableException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.function.EntityResponse;

import java.util.List;

@Slf4j
@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/weather")
public class OpenWeatherMapController {

    @Autowired
    private CurrentWeatherOneLocation currentWeatherOneLocation;

    @Autowired
    private HourlyForecastOneLocation dailyWeatherOneLocation;

    @GetMapping("current/{city}")
    public ResponseEntity<OpenWeatherMapCurrentDto> getCurrentWeatherByCity(@PathVariable String city) throws CityNotFoundException {
        log.info("Get current weather in {}", city);
        try {
            OpenWeatherMapCurrentDto openWeatherMapCurrentDto = currentWeatherOneLocation.getCurrentWeatherByCity(city);
            return new ResponseEntity<>(openWeatherMapCurrentDto, HttpStatus.OK);
        } catch (Exception e) {
            throw new CityNotFoundException("City: " + city + " not found");
        }
    }

    @GetMapping("current/getByCoordinate/longitude/{longitude}/latitude/{latitude}")
    public ResponseEntity<OpenWeatherMapCurrentDto> getCurrentWeatherByCoordinate(@PathVariable String longitude, @PathVariable String latitude) throws WeatherForCoordinatesNotAvailableException {
        log.info("Get current weather on {} {}", longitude, latitude);
        try {
            OpenWeatherMapCurrentDto openWeatherMapCurrentDto = currentWeatherOneLocation.getCurrentWeatherByCoordinates(longitude, latitude);
            return new ResponseEntity<>(openWeatherMapCurrentDto, HttpStatus.OK);
        } catch (Exception e) {
            throw new CityNotFoundException("Weather for " + longitude + " " + latitude + " coordinates is not available");
        }
    }

    @GetMapping("forecast/{city}")
    public ResponseEntity<List<OpenWeatherMapForecastDto>> getDailyWeather(@PathVariable String city) throws CityNotFoundException {
        log.info("Get forecast weather in {}", city);
        try {
            List<OpenWeatherMapForecastDto> openWeatherMapForecastDtos = dailyWeatherOneLocation.getForecastWeatherByCity(city);
            return new ResponseEntity<>(openWeatherMapForecastDtos, HttpStatus.OK);
        } catch (Exception e) {
            throw new CityNotFoundException("City: " + city + " not found");
        }
    }

    @GetMapping("forecast/getByCoordinate/longitude/{longitude}/latitude/{latitude}")
    public ResponseEntity<List<OpenWeatherMapForecastDto>> getForecastWeatherByCoordinate(@PathVariable String longitude, @PathVariable String latitude) throws WeatherForCoordinatesNotAvailableException {
        log.info("Get forecast weather on {} {}", longitude, latitude);
        try {
            List<OpenWeatherMapForecastDto> openWeatherMapForecastDtos = dailyWeatherOneLocation.getForecastWeatherByCoordinates(longitude, latitude);
            return new ResponseEntity<>(openWeatherMapForecastDtos, HttpStatus.OK);
        } catch (Exception e) {
            throw new CityNotFoundException("Weather for " + longitude + " " + latitude + " coordinates is not available");
        }
    }
}
