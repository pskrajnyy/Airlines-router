package com.router.airlines.controller;

import com.router.airlines.client.checkWx.MetarOneLocation;
import com.router.airlines.client.checkWx.TafOneLocation;
import com.router.airlines.dto.checkWx.MetarDto;
import com.router.airlines.dto.checkWx.TafDto;
import com.router.airlines.exception.AirportNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@CrossOrigin(origins = "*")
public class CheckWxController {

    @Autowired
    TafOneLocation tafOneLocation;

    @Autowired
    MetarOneLocation metarOneLocation;

    @GetMapping("taf/encoded/{icao}")
    public ResponseEntity<String> getEncodedTafByIcaoCode(@PathVariable String icao) throws AirportNotFoundException {
        log.info("Get encoded taf on airport: {}", icao);
        try {
            TafDto tafDto = tafOneLocation.getTaf(icao);
            String rawTaf = tafDto.getData().get(0).getRawText();
            return new ResponseEntity<>(rawTaf, HttpStatus.OK);
        } catch (Exception e) {
            throw new AirportNotFoundException("Airport with " + icao + " code not found");
        }
    }

    @GetMapping("taf/decoded/{icao}")
    public ResponseEntity<TafDto> getDecodedTafByIcaoCode(@PathVariable String icao) throws AirportNotFoundException {
        log.info("Get decoded taf on airport: {}", icao);
        try {
            TafDto tafDto = tafOneLocation.getTaf(icao);
            return new ResponseEntity<>(tafDto, HttpStatus.OK);
        } catch (Exception e) {
            throw new AirportNotFoundException("Airport with " + icao + " code not found");
        }
    }

    @GetMapping("metar/encoded/{icao}")
    public ResponseEntity<String> getEncodedMetarByIcaoCode(@PathVariable String icao) throws AirportNotFoundException {
        log.info("Get encoded metar on airport: {}", icao);
        try {
            MetarDto metarDto = metarOneLocation.getMetar(icao);
            String rawMetar = metarDto.getData().get(0).getRawText();
            return new ResponseEntity<>(rawMetar, HttpStatus.OK);
        } catch (Exception e) {
            throw new AirportNotFoundException("Airport with " + icao + " code not found");
        }
    }

    @GetMapping("metar/decoded/{icao}")
    public ResponseEntity<MetarDto> getDecodedMetarByIcaoCode(@PathVariable String icao) throws AirportNotFoundException {
        log.info("Get decoded metar on airport: {}", icao);
        try {
            MetarDto metarDto = metarOneLocation.getMetar(icao);
            return new ResponseEntity<>(metarDto, HttpStatus.OK);
        } catch (Exception e) {
            throw new AirportNotFoundException("Airport with " + icao + " code not found");
        }
    }
}
