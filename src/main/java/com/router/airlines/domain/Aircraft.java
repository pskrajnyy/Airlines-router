package com.router.airlines.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
@Entity
public class Aircraft {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "model")
    private String model;

    @NotNull
    @Column(name = "empty_weight")
    private BigDecimal emptyWeight;

    @NotNull
    @Column(name = "takeoff_landing_weight")
    private BigDecimal takeoffLandingWeight;

    @NotNull
    @Column(name = "max_speed")
    private BigDecimal maxSpeed;

    @NotNull
    @Column(name = "cruising_speed")
    private BigDecimal cruisingSpeed;

    @NotNull
    @Column(name = "fuel_capacity")
    private BigDecimal fuelCapacity;

    @NotNull
    @Column(name = "fuel_burn_per_hour")
    private BigDecimal fuelBurnPerHour;

    @NotNull
    @Column(name = "max_range")
    private BigDecimal maxRange;

}
