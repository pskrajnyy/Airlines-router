package com.router.airlines.dto;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AircraftDto {

    private Long id;

    @NotNull
    private String model;

    @NotNull
    private BigDecimal emptyWeight;

    @NotNull
    private BigDecimal takeoffLandingWeight;

    @NotNull
    private BigDecimal maxSpeed;

    @NotNull
    private BigDecimal cruisingSpeed;

    @NotNull
    private BigDecimal fuelCapacity;

    @NotNull
    private BigDecimal fuelBurnPerHour;

    @NotNull
    private BigDecimal maxRange;
}
