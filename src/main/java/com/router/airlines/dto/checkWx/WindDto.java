package com.router.airlines.dto.checkWx;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.google.gson.annotations.SerializedName;
import lombok.Data;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "speed_mph",
        "speed_mps",
        "speed_kts",
        "degrees"
})
@Data
public class WindDto {

    @JsonProperty("speed_mph")
    @SerializedName("speed_mph")
    private Integer speedMph;

    @JsonProperty("speed_mps")
    @SerializedName("speed_mps")
    private Double speedMps;

    @JsonProperty("speed_kts")
    @SerializedName("speed_kts")
    private Double speedKts;

    @JsonProperty("degrees")
    private Double degrees;
}