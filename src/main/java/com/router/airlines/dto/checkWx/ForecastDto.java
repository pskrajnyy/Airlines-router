package com.router.airlines.dto.checkWx;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.google.gson.annotations.SerializedName;
import lombok.Data;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "visibility",
        "section",
        "clouds",
        "conditions",
        "section_key",
        "wind",
        "timestamp",
        "change"
})
@Data
public class ForecastDto {

    @JsonProperty("visibility")
    private VisibilityDto visibility;

    @JsonProperty("section")
    private String section;

    @JsonProperty("clouds")
    private List<CloudDto> clouds = null;

    @JsonProperty("conditions")
    private List<ConditionDto> conditions = null;

    @JsonProperty("section_key")
    @SerializedName("section_key")
    private String sectionKey;

    @JsonProperty("wind")
    private WindDto wind;

    @JsonProperty("timestamp")
    private TimestampDto timestamp;

    @JsonProperty("change")
    private ChangeDto change;
}