package com.router.airlines.dto.checkWx;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.google.gson.annotations.SerializedName;
import lombok.Data;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "elevation",
        "raw_text",
        "ceiling",
        "flight_category",
        "dewpoint",
        "visibility",
        "barometer",
        "clouds",
        "observed",
        "temperature",
        "station",
        "humidity",
        "icao",
        "location",
        "conditions",
        "wind"
})
@Data
public class MetarDatumDto {

    @JsonProperty("elevation")
    private ElevationDto elevation;

    @JsonProperty("raw_text")
    @SerializedName("raw_text")
    private String rawText;

    @JsonProperty("ceiling")
    private CeilingDto ceiling;

    @JsonProperty("flight_category")
    @SerializedName("flight_category")
    private String flightCategory;

    @JsonProperty("dewpoint")
    private DewpointDto dewpoint;

    @JsonProperty("visibility")
    private VisibilityDto visibility;

    @JsonProperty("barometer")
    private BarometerDto barometer;

    @JsonProperty("clouds")
    private List<CloudDto> clouds = null;

    @JsonProperty("observed")
    private String observed;

    @JsonProperty("temperature")
    private TemperatureDto temperature;

    @JsonProperty("station")
    private StationDto station;

    @JsonProperty("humidity")
    private HumidityDto humidity;

    @JsonProperty("icao")
    private String icao;

    @JsonProperty("location")
    private LocationDto location;

    @JsonProperty("conditions")
    private List<Object> conditions = null;

    @JsonProperty("wind")
    private WindDto wind;
}