package com.router.airlines.dto.checkWx;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "data",
        "results"
})
@Data
public class TafDto {

    @JsonProperty("data")
    private List<TafDatumDto> data = null;

    @JsonProperty("results")
    private Integer results;
}