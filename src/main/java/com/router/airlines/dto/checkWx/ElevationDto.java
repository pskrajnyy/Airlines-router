package com.router.airlines.dto.checkWx;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "feet",
        "meters"
})
@Data
public class ElevationDto {

    @JsonProperty("feet")
    private Double feet;

    @JsonProperty("meters")
    private Double meters;
}