package com.router.airlines.dto.checkWx;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "mb",
        "hpa",
        "kpa",
        "hg"
})
@Data
public class BarometerDto {

    @JsonProperty("mb")
    private Double mb;

    @JsonProperty("hpa")
    private Double hpa;

    @JsonProperty("kpa")
    private Double kpa;

    @JsonProperty("hg")
    private Double hg;
}