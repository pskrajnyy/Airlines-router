package com.router.airlines.dto.checkWx;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.google.gson.annotations.SerializedName;
import lombok.Data;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "indicator",
        "probability",
        "time_becoming"
})
@Data
public class ChangeDto {

    @JsonProperty("indicator")
    private IndicatorDto indicator;

    @JsonProperty("probability")
    private String probability;

    @JsonProperty("time_becoming")
    @SerializedName("time_becoming")
    private String timeBecoming;
}