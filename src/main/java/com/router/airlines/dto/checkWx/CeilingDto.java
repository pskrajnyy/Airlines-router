package com.router.airlines.dto.checkWx;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.google.gson.annotations.SerializedName;
import lombok.Data;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "code",
        "feet_agl",
        "text",
        "meters_agl"
})
@Data
public class CeilingDto {

    @JsonProperty("code")
    private String code;

    @JsonProperty("feet_agl")
    @SerializedName("feet_agl")
    private Double feetAgl;

    @JsonProperty("text")
    private String text;

    @JsonProperty("meters_agl")
    @SerializedName("meters_agl")
    private Double metersAgl;
}