package com.router.airlines.dto.checkWx;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.google.gson.annotations.SerializedName;
import lombok.Data;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "raw_text",
        "station",
        "icao",
        "location",
        "forecast",
        "timestamp"
})
@Data
public class TafDatumDto {

    @JsonProperty("raw_text")
    @SerializedName("raw_text")
    private String rawText;

    @JsonProperty("station")
    private StationDto station;

    @JsonProperty("icao")
    private String icao;

    @JsonProperty("location")
    private LocationDto location;

    @JsonProperty("forecast")
    private List<ForecastDto> forecast = null;

    @JsonProperty("timestamp")
    private TafTimestampDto timestamp;
}