package com.router.airlines.dto.checkWx;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.google.gson.annotations.SerializedName;
import lombok.Data;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "code",
        "base_feet_agl",
        "text",
        "base_meters_agl"
})
@Data
public class CloudDto {

    @JsonProperty("code")
    private String code;

    @JsonProperty("base_feet_agl")
    @SerializedName("base_feet_agl")
    private Double baseFeetAgl;

    @JsonProperty("text")
    private String text;

    @JsonProperty("base_meters_agl")
    @SerializedName("base_meters_agl")
    private Double baseMetersAgl;
}