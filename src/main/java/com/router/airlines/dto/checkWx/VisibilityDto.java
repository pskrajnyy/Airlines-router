package com.router.airlines.dto.checkWx;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.google.gson.annotations.SerializedName;
import lombok.Data;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "meters_float",
        "miles_float",
        "miles",
        "meters"
})
@Data
public class VisibilityDto {

    @JsonProperty("meters_float")
    @SerializedName("meters_float")
    private Double metersFloat;

    @JsonProperty("miles_float")
    @SerializedName("miles_float")
    private Double milesFloat;

    @JsonProperty("miles")
    private String miles;

    @JsonProperty("meters")
    private String meters;
}