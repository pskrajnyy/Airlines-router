package com.router.airlines.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class PrivilegeDto {

    @NotNull
    private String name;
}
