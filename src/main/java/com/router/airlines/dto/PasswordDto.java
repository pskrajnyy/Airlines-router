package com.router.airlines.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class PasswordDto {

    @NotNull
    private String oldPassword;

    @NotNull
    private String newPassword;
}
