package com.router.airlines.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Collection;

@Data
public class RoleDto {

    @NotNull
    private Collection<UserDto> userDto;

    @NotNull
    private Collection<PrivilegeDto> privilegeDto;

    @NotNull
    private String name;
}
