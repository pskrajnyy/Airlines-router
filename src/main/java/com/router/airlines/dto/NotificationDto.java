package com.router.airlines.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class NotificationDto {

    @NotNull
    private UserDto userDto;

    @NotNull
    private String title;

    @NotNull
    private String content;
}
