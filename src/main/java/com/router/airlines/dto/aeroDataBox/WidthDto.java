package com.router.airlines.dto.aeroDataBox;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "feet",
        "km",
        "meter",
        "mile",
        "nm"
})
@Data
public class WidthDto {

    @JsonProperty("feet")
    private Double feet;
    @JsonProperty("km")
    private Double km;
    @JsonProperty("meter")
    private Double meter;
    @JsonProperty("mile")
    private Double mile;
    @JsonProperty("nm")
    private Double nm;
}