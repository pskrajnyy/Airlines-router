package com.router.airlines.dto.aeroDataBox;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "localTime",
        "utcTime"
})
@Data
public class LocalTimeAirportDto {

    @JsonProperty("localTime")
    private String localTime;
    @JsonProperty("utcTime")
    private String utcTime;
}