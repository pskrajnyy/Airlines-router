package com.router.airlines.dto.aeroDataBox;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "hasLighting",
        "isClosed",
        "surface",
        "displacedThreshold",
        "trueHdg",
        "name",
        "length",
        "width",
        "location"
})
@Data
public class RunwaysDto {

    @JsonProperty("hasLighting")
    private Boolean hasLighting;
    @JsonProperty("isClosed")
    private Boolean isClosed;
    @JsonProperty("surface")
    private String surface;
    @JsonProperty("displacedThreshold")
    private DisplacedThresholdDto displacedThreshold;
    @JsonProperty("trueHdg")
    private Double trueHdg;
    @JsonProperty("name")
    private String name;
    @JsonProperty("length")
    private LengthDto length;
    @JsonProperty("width")
    private WidthDto width;
    @JsonProperty("location")
    private LocationDto location;
}