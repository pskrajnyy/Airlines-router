package com.router.airlines.dto.aeroDataBox;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "continent",
        "country",
        "urls",
        "iata",
        "icao",
        "fullName",
        "timeZone",
        "location",
        "shortName",
        "municipalityName"
})
@Data
public class AirportBasicInformationDto {

    @JsonProperty("continent")
    private ContinentDto continent;
    @JsonProperty("country")
    private CountryDto country;
    @JsonProperty("urls")
    private UrlsDto urls;
    @JsonProperty("iata")
    private String iata;
    @JsonProperty("icao")
    private String icao;
    @JsonProperty("fullName")
    private String fullName;
    @JsonProperty("timeZone")
    private String timeZone;
    @JsonProperty("location")
    private LocationDto location;
    @JsonProperty("shortName")
    private String shortName;
    @JsonProperty("municipalityName")
    private String municipalityName;
}