package com.router.airlines.dto.aeroDataBox;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "webSite",
        "twitter",
        "flightRadar",
        "wikipedia",
        "googleMaps",
        "liveAtc"
})
@Data
public class UrlsDto {

    @JsonProperty("webSite")
    private String webSite;
    @JsonProperty("twitter")
    private String twitter;
    @JsonProperty("flightRadar")
    private String flightRadar;
    @JsonProperty("wikipedia")
    private String wikipedia;
    @JsonProperty("googleMaps")
    private String googleMaps;
    @JsonProperty("liveAtc")
    private String liveAtc;
}