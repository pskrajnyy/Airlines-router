package com.router.airlines.dto.aeroDataBox;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "code",
        "name"
})
@Data
public class CountryDto {

    @JsonProperty("code")
    private String code;
    @JsonProperty("name")
    private String name;
}