package com.router.airlines.client.openWeatherMap;

import com.router.airlines.config.OpenWeatherMapConfig;
import com.router.airlines.dto.openWeatherMap.OpenWeatherMapCurrentDto;
import org.openweathermap.api.DataWeatherClient;
import org.openweathermap.api.UrlConnectionDataWeatherClient;
import org.openweathermap.api.common.Coordinate;
import org.openweathermap.api.model.currentweather.CurrentWeather;
import org.openweathermap.api.query.*;
import org.openweathermap.api.query.currentweather.CurrentWeatherOneLocationQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CurrentWeatherOneLocation {

    @Autowired
    private OpenWeatherMapConfig openWeatherMapConfig;

    public OpenWeatherMapCurrentDto getCurrentWeatherByCity(String city) {
        DataWeatherClient client = new UrlConnectionDataWeatherClient(openWeatherMapConfig.getAppKey());
        CurrentWeatherOneLocationQuery currentWeatherOneLocationQuery = QueryBuilderPicker.pick()
                .currentWeather()
                .oneLocation()
                .byCityName(city)
                .type(Type.ACCURATE)
                .language(Language.ENGLISH)
                .responseFormat(ResponseFormat.JSON)
                .unitFormat(UnitFormat.METRIC)
                .build();
        CurrentWeather currentWeather = client.getCurrentWeather(currentWeatherOneLocationQuery);

        return mapToDto(currentWeather);
    }

    public OpenWeatherMapCurrentDto getCurrentWeatherByCoordinates(String longitude, String latitude) {
        DataWeatherClient client = new UrlConnectionDataWeatherClient(openWeatherMapConfig.getAppKey());
        Coordinate coordinate = new Coordinate(longitude, latitude);
        CurrentWeatherOneLocationQuery currentWeatherOneLocationQuery = QueryBuilderPicker.pick()
                .currentWeather()
                .oneLocation()
                .byGeographicCoordinates(coordinate)
                .language(Language.ENGLISH)
                .responseFormat(ResponseFormat.JSON)
                .unitFormat(UnitFormat.METRIC)
                .build();
        CurrentWeather currentWeather = client.getCurrentWeather(currentWeatherOneLocationQuery);

        return mapToDto(currentWeather);
    }

    private OpenWeatherMapCurrentDto mapToDto(CurrentWeather currentWeather) {
        return new OpenWeatherMapCurrentDto(currentWeather.getCityName(), currentWeather.getMainParameters().getTemperature(),
                currentWeather.getMainParameters().getHumidity(),
                currentWeather.getMainParameters().getPressure(),
                currentWeather.getMainParameters().getMinimumTemperature(),
                currentWeather.getMainParameters().getMaximumTemperature(),
                currentWeather.getWind().getSpeed(),
                currentWeather.getWind().getDirection().getDegree());
    }
}
