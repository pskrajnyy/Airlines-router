package com.router.airlines.client.openWeatherMap;

import com.router.airlines.config.OpenWeatherMapConfig;
import com.router.airlines.dto.openWeatherMap.OpenWeatherMapForecastDto;
import org.openweathermap.api.DataWeatherClient;
import org.openweathermap.api.UrlConnectionDataWeatherClient;
import org.openweathermap.api.common.Coordinate;
import org.openweathermap.api.model.forecast.ForecastInformation;
import org.openweathermap.api.model.forecast.hourly.HourlyForecast;
import org.openweathermap.api.query.Language;
import org.openweathermap.api.query.QueryBuilderPicker;
import org.openweathermap.api.query.UnitFormat;
import org.openweathermap.api.query.forecast.hourly.ByCityName;
import org.openweathermap.api.query.forecast.hourly.ByGeographicCoordinates;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class HourlyForecastOneLocation {

    @Autowired
    private OpenWeatherMapConfig openWeatherMapConfig;

    public List<OpenWeatherMapForecastDto> getForecastWeatherByCity(String city) {
        DataWeatherClient client = new UrlConnectionDataWeatherClient(openWeatherMapConfig.getAppKey());
        ByCityName byCityNameForecast = QueryBuilderPicker.pick()
                .forecast()
                .hourly()
                .byCityName(city)
                .unitFormat(UnitFormat.METRIC)
                .language(Language.ENGLISH)
                .count(5)
                .build();
        ForecastInformation<HourlyForecast> forecastInformation = client.getForecastInformation(byCityNameForecast);

        return mapToDtos(forecastInformation);
    }

    public List<OpenWeatherMapForecastDto> getForecastWeatherByCoordinates(String longitude, String latitude) {
        DataWeatherClient client = new UrlConnectionDataWeatherClient(openWeatherMapConfig.getAppKey());
        Coordinate coordinate = new Coordinate(longitude, latitude);
        ByGeographicCoordinates byGeographicCoordinates = QueryBuilderPicker.pick()
                .forecast()
                .hourly()
                .byGeographicCoordinates(coordinate)
                .unitFormat(UnitFormat.METRIC)
                .language(Language.ENGLISH)
                .count(5)
                .build();
        ForecastInformation<HourlyForecast> forecastInformation = client.getForecastInformation(byGeographicCoordinates);

        return mapToDtos(forecastInformation);
    }

    private List<OpenWeatherMapForecastDto> mapToDtos(ForecastInformation<HourlyForecast> forecastInformation) {
        List<OpenWeatherMapForecastDto> result = new ArrayList<>();
        for (HourlyForecast forecast : forecastInformation.getForecasts()) {
            OpenWeatherMapForecastDto openWeatherMapForecastDto = new OpenWeatherMapForecastDto(forecast.getDateTime().toString(),
                    forecastInformation.getCity().getName(),
                    forecast.getMainParameters().getTemperature(),
                    forecast.getMainParameters().getHumidity(),
                    forecast.getMainParameters().getPressure(),
                    forecast.getMainParameters().getMinimumTemperature(),
                    forecast.getMainParameters().getMaximumTemperature(),
                    forecast.getWind().getSpeed(),
                    forecast.getWind().getDirection().getDegree());
            result.add(openWeatherMapForecastDto);
        }
        return result;
    }
}
