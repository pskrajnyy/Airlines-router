package com.router.airlines.client.aeroDataBox;

import com.google.gson.Gson;
import com.router.airlines.config.AeroDataBoxConfig;
import com.router.airlines.dto.aeroDataBox.RunwaysDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URL;

@Component
public class RunwaysInformationAirport {

    @Autowired
    AeroDataBoxConfig aeroDataBoxConfig;

    public RunwaysDto[] getRunwayInformationAirportByIcao(String icao) throws IOException {

        URL url = new URL(aeroDataBoxConfig.getGetBasicInformationUrl() + "icao/" + icao + "/runways");
        String response = aeroDataBoxConfig.getResponseAeroDataBox(url);
        Gson gson = new Gson();
        RunwaysDto[] runwaysDtos = gson.fromJson(response, RunwaysDto[].class);
        return runwaysDtos;
    }
}
