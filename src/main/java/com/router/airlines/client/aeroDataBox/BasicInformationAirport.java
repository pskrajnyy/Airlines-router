package com.router.airlines.client.aeroDataBox;

import com.google.gson.Gson;
import com.router.airlines.config.AeroDataBoxConfig;
import com.router.airlines.dto.aeroDataBox.AirportBasicInformationDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URL;

@Component
public class BasicInformationAirport {

    @Autowired
    AeroDataBoxConfig aeroDataBoxConfig;

    public AirportBasicInformationDto getBasicInformationAboutAirport(String typeCode, String code) throws IOException {
        URL url = new URL(aeroDataBoxConfig.getGetBasicInformationUrl() + typeCode + "/" + code);
        String response = aeroDataBoxConfig.getResponseAeroDataBox(url);
        Gson gson = new Gson();
        return gson.fromJson(response, AirportBasicInformationDto.class);
    }
}
