package com.router.airlines.client.aeroDataBox;

import com.google.gson.Gson;
import com.router.airlines.config.AeroDataBoxConfig;
import com.router.airlines.dto.aeroDataBox.LocalTimeAirportDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URL;

@Component
public class LocalTimeAirport {

    @Autowired
    AeroDataBoxConfig aeroDataBoxConfig;

    public LocalTimeAirportDto getLocalTimeAirport(String code) throws IOException {
        URL url = new URL(aeroDataBoxConfig.getGetBasicInformationUrl() + "icao/" + code + "/time/local");
        String response = aeroDataBoxConfig.getResponseAeroDataBox(url);
        Gson gson = new Gson();
        return gson.fromJson(response, LocalTimeAirportDto.class);
    }
}