package com.router.airlines.client.checkWx;

import com.google.gson.Gson;
import com.router.airlines.config.CheckWxConfig;
import com.router.airlines.dto.checkWx.TafDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URL;

@Component
public class TafOneLocation {

    @Autowired
    private CheckWxConfig checkWxConfig;

    public TafDto getTaf(String icao) throws IOException {
        URL url = new URL(checkWxConfig.getTafUrl() + icao + "/decoded");
        String response = checkWxConfig.getResponseCheckWx(url);
        Gson gson = new Gson();
        return gson.fromJson(response, TafDto.class);
    }
}
