package com.router.airlines.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan({ "com.router.airlines.service" })
public class ServiceConfig {
}
