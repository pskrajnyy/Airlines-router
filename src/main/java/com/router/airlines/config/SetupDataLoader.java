package com.router.airlines.config;

import com.router.airlines.domain.Aircraft;
import com.router.airlines.domain.Privilege;
import com.router.airlines.domain.Role;
import com.router.airlines.domain.User;
import com.router.airlines.repository.AircraftRepository;
import com.router.airlines.repository.PrivilegeRepository;
import com.router.airlines.repository.RoleRepository;
import com.router.airlines.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

@Component
public class SetupDataLoader implements ApplicationListener<ContextRefreshedEvent> {

    private boolean alreadySetup = false;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PrivilegeRepository privilegeRepository;

    @Autowired
    private AircraftRepository aircraftRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    @Transactional
    public void onApplicationEvent(final ContextRefreshedEvent event) {
        if (alreadySetup) {
            return;
        }

        // == create initial privileges
        final Privilege readPrivilege = createPrivilegeIfNotFound("READ_PRIVILEGE");
        final Privilege writePrivilege = createPrivilegeIfNotFound("WRITE_PRIVILEGE");
        final Privilege passwordPrivilege = createPrivilegeIfNotFound("CHANGE_PASSWORD_PRIVILEGE");

        // == create initial roles
        final List<Privilege> adminPrivileges = new ArrayList<Privilege>(Arrays.asList(readPrivilege, writePrivilege, passwordPrivilege));
        final List<Privilege> userPrivileges = new ArrayList<Privilege>(Arrays.asList(readPrivilege, passwordPrivilege));
        final Role adminRole = createRoleIfNotFound("ROLE_ADMIN", adminPrivileges);
        final Role userRole = createRoleIfNotFound("ROLE_USER", userPrivileges);

        // == create initial user
        createUserIfNotFound("admin@admin.pl", "Admin", "Admin", "admin", LocalDate.now(), new ArrayList<Role>(Arrays.asList(adminRole)));
        createUserIfNotFound("user@user.com", "User", "User", "user", LocalDate.now(), new ArrayList<Role>(Arrays.asList(userRole)));

        // == create initial aircraft
        createAircraftIfNotFound("Cessna 152", new BigDecimal(515), new BigDecimal(758), new BigDecimal(276), new BigDecimal(198), new BigDecimal(93), new BigDecimal(23.09), new BigDecimal(592));
        createAircraftIfNotFound("Cessna 172", new BigDecimal(743), new BigDecimal(1100), new BigDecimal(302), new BigDecimal(226), new BigDecimal(200), new BigDecimal(31.79), new BigDecimal(959.3));
        createAircraftIfNotFound("Cirrus Vision SF50", new BigDecimal(1620), new BigDecimal(2722), new BigDecimal(560), new BigDecimal(280), new BigDecimal(2000), new BigDecimal(462), new BigDecimal(1100));
        alreadySetup = true;
    }

    @Transactional
    Privilege createPrivilegeIfNotFound(final String name) {
        Privilege privilege = privilegeRepository.findByName(name);
        if (privilege == null) {
            privilege = new Privilege(name);
            privilege = privilegeRepository.save(privilege);
        }
        return privilege;
    }

    @Transactional
    Role createRoleIfNotFound(final String name, final Collection<Privilege> privileges) {
        Role role = roleRepository.findByName(name);
        if (role == null) {
            role = new Role(name);
        }
        role.setPrivileges(privileges);
        role = roleRepository.save(role);
        return role;
    }

    @Transactional
    User createUserIfNotFound(final String email, final String firstName, final String lastName, final String password, final LocalDate registrationDate, final Collection<Role> roles) {
        User user = userRepository.findByEmail(email);
        if (user == null) {
            user = new User();
            user.setFirstName(firstName);
            user.setLastName(lastName);
            user.setPassword(passwordEncoder.encode(password));
            user.setEmail(email);
            user.setEnabled(true);
            user.setRegistrationDate(registrationDate);
        }
        user.setRoles(roles);
        user = userRepository.save(user);
        return user;
    }

    @Transactional
    Aircraft createAircraftIfNotFound(final String model, final BigDecimal emptyWeight, final BigDecimal takeoffLandingWeight, final BigDecimal maxSpeed, final BigDecimal cruisingSpeed, final BigDecimal fuelCapacity, final BigDecimal fuelBurnPerHour, final BigDecimal maxRange) {
        Aircraft aircraft = aircraftRepository.getAircraftByModel(model);
        if(aircraft == null) {
            aircraft = new Aircraft();
            aircraft.setModel(model);
            aircraft.setEmptyWeight(emptyWeight);
            aircraft.setTakeoffLandingWeight(takeoffLandingWeight);
            aircraft.setMaxSpeed(maxSpeed);
            aircraft.setCruisingSpeed(cruisingSpeed);
            aircraft.setFuelCapacity(fuelCapacity);
            aircraft.setFuelBurnPerHour(fuelBurnPerHour);
            aircraft.setMaxRange(maxRange);
        }
        aircraft = aircraftRepository.save(aircraft);
        return aircraft;
    }
}