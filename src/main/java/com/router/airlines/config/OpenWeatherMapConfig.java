package com.router.airlines.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Data
@Component
public class OpenWeatherMapConfig {

	@Value(value = "${open.weather.map.app.key}")
	private String appKey;
}
