package com.router.airlines.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

@Data
@Component
public class CheckWxConfig {

    @Value(value = "${check.wx.app.key}")
    private String appKey;

    @Value(value = "${check.wx.taf.url}")
    private String tafUrl;

    @Value(value = "${check.wx.metar.url}")
    private String metarUrl;

    public String getResponseCheckWx(URL url) throws IOException {
        HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
        httpURLConnection.setRequestMethod("GET");
        httpURLConnection.setRequestProperty("X-API-Key", getAppKey());
        httpURLConnection.setDoOutput(true);

        BufferedReader in = new BufferedReader(
                new InputStreamReader(httpURLConnection.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();
        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        return String.valueOf(response);
    }
}
