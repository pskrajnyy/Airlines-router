package com.router.airlines.exception;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class InvalidOldPasswordException extends RuntimeException {

    private static final long serialVersionUID = 5861310537366287163L;

    public InvalidOldPasswordException() {
        super();
    }

    public InvalidOldPasswordException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public InvalidOldPasswordException(final String message) {
        log.error(message);
    }

    public InvalidOldPasswordException(final Throwable cause) {
        super(cause);
    }

}
