package com.router.airlines.exception;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class VerificationTokenExpiredException extends RuntimeException {

    public VerificationTokenExpiredException() {
        super();
    }

    public VerificationTokenExpiredException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public VerificationTokenExpiredException(final String message) {
        log.error(message);
    }

    public VerificationTokenExpiredException(final Throwable cause) {
        super(cause);
    }

}
