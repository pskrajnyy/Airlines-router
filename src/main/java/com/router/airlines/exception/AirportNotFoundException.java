package com.router.airlines.exception;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class AirportNotFoundException extends RuntimeException {

    private static final long serialVersionUID = 5861310537366287163L;

    public AirportNotFoundException() {
        super();
    }

    public AirportNotFoundException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public AirportNotFoundException(final String message) {
        log.error(message);
    }

    public AirportNotFoundException(final Throwable cause) {
        super(cause);
    }

}