package com.router.airlines.exception;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class VerificationTokenInvalidException extends RuntimeException {

    public VerificationTokenInvalidException() {
        super();
    }

    public VerificationTokenInvalidException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public VerificationTokenInvalidException(final String message) {
        log.error(message);
    }

    public VerificationTokenInvalidException(final Throwable cause) {
        super(cause);
    }

}