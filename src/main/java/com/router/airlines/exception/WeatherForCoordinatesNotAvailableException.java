package com.router.airlines.exception;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class WeatherForCoordinatesNotAvailableException extends RuntimeException {

    private static final long serialVersionUID = 5861310537366287163L;

    public WeatherForCoordinatesNotAvailableException() {
        super();
    }

    public WeatherForCoordinatesNotAvailableException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public WeatherForCoordinatesNotAvailableException(final String message) {
        log.error(message);
    }

    public WeatherForCoordinatesNotAvailableException(final Throwable cause) {
        super(cause);
    }

}
