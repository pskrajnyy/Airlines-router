package com.router.airlines.exception;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class UserIsEnabledException extends RuntimeException {

    public UserIsEnabledException() {
        super();
    }

    public UserIsEnabledException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public UserIsEnabledException(final String message) {
        log.error(message);
    }

    public UserIsEnabledException(final Throwable cause) {
        super(cause);
    }

}
