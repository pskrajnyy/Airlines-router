package com.router.airlines.exception;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class AircraftNotFoundException extends RuntimeException {

    private static final long serialVersionUID = 5861310537366287163L;

    public AircraftNotFoundException() {
        super();
    }

    public AircraftNotFoundException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public AircraftNotFoundException(final String message) {
        log.error(message);
    }

    public AircraftNotFoundException(final Throwable cause) {
        super(cause);
    }

}