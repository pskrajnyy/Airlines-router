package com.router.airlines.exception;

import com.router.airlines.util.GenericResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.MailAuthenticationException;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    @Autowired
    private MessageSource messages;

    public RestResponseEntityExceptionHandler() {
        super();
    }

    @Override
    protected ResponseEntity<Object> handleBindException(final BindException ex, final HttpHeaders headers, final HttpStatus status, final WebRequest request) {
        logger.error("400 Status Code", ex);
        final BindingResult result = ex.getBindingResult();
        final GenericResponse bodyOfResponse = new GenericResponse(result.getAllErrors(), "Invalid" + result.getObjectName());
        return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(final MethodArgumentNotValidException ex, final HttpHeaders headers, final HttpStatus status, final WebRequest request) {
        logger.error("400 Status Code", ex);
        final BindingResult result = ex.getBindingResult();
        final GenericResponse bodyOfResponse = new GenericResponse(result.getAllErrors(), "Invalid" + result.getObjectName());
        return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

    @ExceptionHandler(MailAuthenticationException.class)
    public ResponseEntity<Object> handleMail(final RuntimeException ex, final WebRequest request) {
        logger.error("500 Status Code", ex);
        final GenericResponse bodyOfResponse = new GenericResponse(messages.getMessage("message.email.config.error", null, request.getLocale()), "MailError");
        return new ResponseEntity<Object>(bodyOfResponse, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> handleInternal(final RuntimeException ex, final WebRequest request) {
        logger.error("500 Status Code", ex);
        final GenericResponse bodyOfResponse = new GenericResponse(messages.getMessage("message.error", null, request.getLocale()), "InternalError");
        return new ResponseEntity<Object>(bodyOfResponse, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(AircraftNotFoundException.class)
    public ResponseEntity<String> handleAircraftNotFoundException(final RuntimeException ex, final WebRequest request) {
        return new ResponseEntity<>(messages.getMessage("message.aircraftNotFound", null, null), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(AirportNotFoundException.class)
    public ResponseEntity<String> handleAirportNotFoundException(final RuntimeException ex, final WebRequest request) {
        return new ResponseEntity<>(messages.getMessage("message.airportNotFound", null, null), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(CityNotFoundException.class)
    public ResponseEntity<String> handleCityNotFoundException(final RuntimeException ex, final WebRequest request) {
        return new ResponseEntity<>(messages.getMessage("message.cityNotFound", null, null), HttpStatus.NOT_FOUND);
    }

//    @ExceptionHandler(EmailExistsException.class)
//    public ResponseEntity<String> handleEmailExistsException(final RuntimeException ex, final WebRequest request) {
//        return new ResponseEntity<>(messages.getMessage("message.airportNotFound", null, null), HttpStatus.OK);
//    }

    @ExceptionHandler(InvalidOldPasswordException.class)
    public ResponseEntity<String> handleInvalidOldPassword(final RuntimeException ex, final WebRequest request) {
        return new ResponseEntity<>(messages.getMessage("message.invalidOldPassword", null, null), HttpStatus.OK);
    }

    @ExceptionHandler(UserAlreadyExistException.class)
    public ResponseEntity<String> handleUserAlreadyExist(final RuntimeException ex, final WebRequest request) {
        return new ResponseEntity<>(messages.getMessage("message.regError", null, null), HttpStatus.OK);
    }

    @ExceptionHandler(UserIsEnabledException.class)
    public ResponseEntity<String> handleUserIsEnabledException(final RuntimeException ex, final WebRequest request) {
        return new ResponseEntity<>(messages.getMessage("message.userIsEnabled", null, null), HttpStatus.OK);
    }

    @ExceptionHandler(UserNotFoundException.class)
    public ResponseEntity<String> handleUserNotFound(final RuntimeException ex, final WebRequest request) {
        return new ResponseEntity<>(messages.getMessage("message.userNotFound", null, null), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(VerificationTokenExpiredException.class)
    public ResponseEntity<String> handleVerificationTokenExpired(final RuntimeException ex, final WebRequest request) {
        return new ResponseEntity<>(messages.getMessage("message.verTokenExpired", null, null), HttpStatus.OK);
    }

    @ExceptionHandler(VerificationTokenInvalidException.class)
    public ResponseEntity<String> handleVerificationTokenInvalid(final RuntimeException ex, final WebRequest request) {
        return new ResponseEntity<>(messages.getMessage("message.verTokenInvalid", null, null), HttpStatus.OK);
    }

    @ExceptionHandler(WeatherForCoordinatesNotAvailableException.class)
    public ResponseEntity<String> handleWeatherForCoordinatesNotAvailableException(final RuntimeException ex, final WebRequest request) {
        return new ResponseEntity<>(messages.getMessage("message.wrongCoordinates", null, null), HttpStatus.NOT_FOUND);
    }
}
