package com.router.airlines.exception;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CityNotFoundException extends RuntimeException {

    private static final long serialVersionUID = 5861310537366287163L;

    public CityNotFoundException() {
        super();
    }

    public CityNotFoundException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public CityNotFoundException(final String message) {
        log.error(message);
    }

    public CityNotFoundException(final Throwable cause) {
        super(cause);
    }

}