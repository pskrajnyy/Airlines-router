package com.router.airlines.service.implementation;

import com.router.airlines.config.jwt.UserPrinciple;
import com.router.airlines.domain.User;
import com.router.airlines.exception.UserNotFoundException;
import com.router.airlines.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    UserRepository userRepository;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String email) throws UserNotFoundException {
        User user = userRepository.findByEmail(email);
        return UserPrinciple.build(user);
    }
}