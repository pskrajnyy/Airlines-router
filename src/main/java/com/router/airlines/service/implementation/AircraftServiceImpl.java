package com.router.airlines.service.implementation;

import com.router.airlines.domain.Aircraft;
import com.router.airlines.repository.AircraftRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service("aircraftService")
@Transactional
public class AircraftServiceImpl implements com.router.airlines.service.AircraftService {

    @Autowired
    private AircraftRepository aircraftRepository;

    @Override
    public List<Aircraft> getAllAircrafts() {
        return aircraftRepository.findAll();
    }

    @Override
    public Aircraft getAircraft(Long aircraftId) {
        return aircraftRepository.getById(aircraftId);
    }
    @Override
    public Aircraft saveAircraft(Aircraft aircraft) {
        return aircraftRepository.save(aircraft);
    }

    @Override
    public void deleteById(Long aircraftId) {
        aircraftRepository.deleteById(aircraftId);
    }
}
