package com.router.airlines.service.implementation;

import com.router.airlines.domain.PasswordResetToken;
import com.router.airlines.domain.User;
import com.router.airlines.domain.VerificationToken;
import com.router.airlines.dto.UserDto;
import com.router.airlines.exception.UserAlreadyExistException;
import com.router.airlines.repository.PasswordResetTokenRepository;
import com.router.airlines.repository.RoleRepository;
import com.router.airlines.repository.UserRepository;
import com.router.airlines.repository.VerificationTokenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Optional;
import java.util.UUID;

@Service("userService")
@Transactional
public class UserServiceImpl implements com.router.airlines.service.UserService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private VerificationTokenRepository verificationTokenRepository;

	@Autowired
	private PasswordResetTokenRepository passwordResetTokenRepository;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private RoleRepository roleRepository;

	public static final String TOKEN_INVALID = "invalidToken";
	public static final String TOKEN_EXPIRED = "expired";
	public static final String TOKEN_VALID = "valid";


	public User registerNewUserAccount(final UserDto accountDto) {
		if (emailExists(accountDto.getEmail())) {
			throw new UserAlreadyExistException("There is an account with that email address: " + accountDto.getEmail());
		}
		final User user = new User();

		user.setFirstName(accountDto.getFirstName());
		user.setLastName(accountDto.getLastName());
		user.setPassword(passwordEncoder.encode(accountDto.getPassword()));
		user.setEmail(accountDto.getEmail());
		user.setRoles(Arrays.asList(roleRepository.findByName("ROLE_USER")));
		user.setRegistrationDate(LocalDate.now());
		return userRepository.save(user);
	}

	@Override
	public String validateVerificationToken(String token) {
		final VerificationToken verificationToken = verificationTokenRepository.findByToken(token);
		if (verificationToken == null) {
			return TOKEN_INVALID;
		}

		final User user = verificationToken.getUser();
		final Calendar cal = Calendar.getInstance();
		if ((verificationToken.getExpiryDate().getTime() - cal.getTime().getTime()) <= 0) {
			verificationTokenRepository.delete(verificationToken);
			return TOKEN_EXPIRED;
		}

		user.setEnabled(true);
		userRepository.save(user);
		return TOKEN_VALID;
	}

	@Override
	public User findUserByEmail(final String email) {
		return userRepository.findByEmail(email);
	}

	@Override
	public User getUser(final String verificationToken) {
		final VerificationToken token = verificationTokenRepository.findByToken(verificationToken);
		if (token != null) {
			return token.getUser();
		}
		return null;
	}

	@Override
	public VerificationToken getVerificationToken(final String VerificationToken) {
		return verificationTokenRepository.findByToken(VerificationToken);
	}

	@Override
	public void saveRegisteredUser(final User user) {
		userRepository.save(user);
	}

	@Override
	public void deleteUser(final User user) {
		final VerificationToken verificationToken = verificationTokenRepository.findByUser(user);

		if (verificationToken != null) {
			verificationTokenRepository.delete(verificationToken);
		}

		final PasswordResetToken passwordToken = passwordResetTokenRepository.findByUser(user);

		if (passwordToken != null) {
			passwordResetTokenRepository.delete(passwordToken);
		}

		userRepository.delete(user);
	}

	@Override
	public void createVerificationTokenForUser(final User user, final String token) {
		final VerificationToken myToken = new VerificationToken(token, user);
		verificationTokenRepository.save(myToken);
	}

	@Override
	public VerificationToken generateNewVerificationToken(final String existingVerificationToken) {
		VerificationToken vToken = verificationTokenRepository.findByToken(existingVerificationToken);
		vToken.updateToken(UUID.randomUUID().toString());
		vToken = verificationTokenRepository.save(vToken);
		return vToken;
	}

	@Override
	public void createPasswordResetTokenForUser(final User user, final String token) {
		final PasswordResetToken myToken = new PasswordResetToken(token, user);
		passwordResetTokenRepository.save(myToken);
	}

	@Override
	public PasswordResetToken getPasswordResetToken(final String token) {
		return passwordResetTokenRepository.findByToken(token);
	}

	public VerificationToken getVerificationTokenByUser(final User user) {
		return verificationTokenRepository.findByUser(user);
	}

	@Override
	public User getUserByPasswordResetToken(final String token) {
		return passwordResetTokenRepository.findByToken(token).getUser();
	}

	@Override
	public Optional<User> getUserByID(final long id) {
		return userRepository.findById(id);
	}

	@Override
	public void changeUserPassword(final User user, final String password) {
		user.setPassword(passwordEncoder.encode(password));
		userRepository.save(user);
	}

	@Override
	public boolean checkIfValidOldPassword(final User user, final String oldPassword) {
		return passwordEncoder.matches(oldPassword, user.getPassword());
	}

	private boolean emailExists(final String email) {
		return userRepository.findByEmail(email) != null;
	}
}
