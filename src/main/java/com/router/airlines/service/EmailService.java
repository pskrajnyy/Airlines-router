package com.router.airlines.service;

public interface EmailService {
    void sendEmail(String to, String subject, String content);
}
