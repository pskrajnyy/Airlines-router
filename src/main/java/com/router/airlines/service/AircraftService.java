package com.router.airlines.service;

import com.router.airlines.domain.Aircraft;

import java.util.List;

public interface AircraftService {

    List<Aircraft> getAllAircrafts();
    Aircraft getAircraft(Long aircraftId);
    Aircraft saveAircraft(Aircraft aircraft);
    void deleteById(Long aircraftId);
}
