package com.router.airlines.service;

import com.router.airlines.domain.PasswordResetToken;
import com.router.airlines.domain.User;
import com.router.airlines.domain.VerificationToken;
import com.router.airlines.dto.UserDto;
import com.router.airlines.exception.UserAlreadyExistException;

import java.util.Optional;

public interface UserService {
    User registerNewUserAccount(UserDto accountDto) throws UserAlreadyExistException;
    String validateVerificationToken(String token);
    User findUserByEmail(String email);
    User getUser(String verificationToken);
    VerificationToken getVerificationToken(String VerificationToken);
    void saveRegisteredUser(User user);
    void deleteUser(User user);
    void createVerificationTokenForUser(User user, String token);
    VerificationToken generateNewVerificationToken(String token);
    void createPasswordResetTokenForUser(User user, String token);
    PasswordResetToken getPasswordResetToken(String token);
    VerificationToken getVerificationTokenByUser(User user);
    User getUserByPasswordResetToken(String token);
    Optional<User> getUserByID(long id);
    void changeUserPassword(User user, String password);
    boolean checkIfValidOldPassword(User user, String password);
}
