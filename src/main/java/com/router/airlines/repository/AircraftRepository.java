package com.router.airlines.repository;

import com.router.airlines.domain.Aircraft;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AircraftRepository extends JpaRepository<Aircraft, Long> {
    void delete(Aircraft aircraft);
    Aircraft getAircraftByModel(String model);
    Aircraft getById(Long aircraftId);
}

